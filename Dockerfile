# (C) Copyright Banco do Brasil 2019.
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
FROM atf.intranet.bb.com.br:5001/bb/lnx/lnx-python3-centos:3.6.6

ARG build_date
ARG vcs_ref
ARG VERSAO=0.0.1
ARG CENTOS_VERSAO=7.6.1810.9
ARG BOM_PATH="/docker/vmw"

LABEL \
    br.com.bb.image.so.sigla="VMW" \
    br.com.bb.image.so.provider="CentOS" \
    br.com.bb.image.so.arch="x86_64" \
    br.com.bb.image.so.maintainer="Banco do Brasil S.A. / DITEC / GESIT <ditec.gesit1.div6@bb.com.br>" \
    br.com.bb.image.so.version="$CENTOS_VERSAO" \
    br.com.bb.image.description="APIs Base Image" \
    org.label-schema.maintainer="Banco do Brasil S.A. / DITEC / GESIT <ditec.gesit1.div6@bb.com.br>" \
    org.label-schema.vendor="Banco do Brasil" \
    org.label-schema.url="https://gitlab.intranet.bb.com.br/vmw/docker-images/vmw-api-base" \
    org.label-schema.name="" \
    org.label-schema.license="COPYRIGHT" \
    org.label-schema.version="$VERSAO" \
    org.label-schema.vcs-url="https://gitlab.intranet.bb.com.br/vmw/docker-images/vmw-api-base" \
    org.label-schema.vcs-ref="$vcs_ref" \
    org.label-schema.build-date="$build_date" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.dockerfile="${BOM_PATH}/Dockerfile"

COPY requirements.txt .
COPY exporter.py . 

ENV PYCURL_SSL_LIBRARY=nss

RUN yum -y install python36-devel libcurl-devel gcc && \
    yum clean all && \
    rm -rf /var/cache/yum && \
    pip3 install -U pip && \
    pip3 install --no-cache-dir -r /requirements.txt


EXPOSE 8000
ENTRYPOINT [ "python3", "-u", "exporter.py" ]
