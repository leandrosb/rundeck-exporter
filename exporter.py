#!/usr/bin/env python3.6
# -*- coding:utf-8 -*-
import prometheus_client
from prometheus_client import Gauge, Info
from prometheus_client.core import CollectorRegistry
from flask import Response, Flask

app = Flask(__name__)
REGISTRY = CollectorRegistry(auto_describe=False)
mem = Gauge('memory', 'memory info',['memtype'],registry=REGISTRY)
i = Info('my_build_version', 'Description of info',registry=REGISTRY)

@app.route("/metrics")
def hahah():
    mem.labels(memtype='total').set(1000)
    mem.labels(memtype='available').set(700)
    mem.labels(memtype='used').set(300)
    i.info({'version': '1.2.3', 'buildhost': 'foo@bar'})
    return Response(prometheus_client.generate_latest(REGISTRY), mimetype="text/plain")

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8000)